import java.util.ArrayList;
import java.util.List;

public class AgeEncoding {
	
	public double getRadix(int age, String candlesLine) {
		int cSum = 0;
		List<Integer> cs = new ArrayList<Integer>();
		for(int i = 0; i < candlesLine.length(); i++) {
			if(candlesLine.charAt(candlesLine.length() - i - 1) == '1') {
				cs.add(i);
				cSum += i + 1;
			}
		}
		if(age == 1 && cSum == 1) return -2;
		// if(age == 1 && cs.size() != 1) return -1;
		if(cSum <= 1) return -1;
		return binSearch(cs, 0, age, age);
	}

	public double binSearch(List<Integer> cs, double a, double b, double target) {
		while (Math.abs(a - b) >= 1e-10) {
			double curr = (a + b) / 2;
			if (func(curr, cs) > target) {
				b = curr;
			} else {
				a = curr;
			}
		}
		return (a + b) / 2;
	}
	
	public double func(double base, List<Integer> cs) {
		double x = 0.0;
		for (int c : cs) {
			x += Math.pow(base, c);
		}
		return x;
	}
}
