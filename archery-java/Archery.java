public class Archery {

	public double expectedPoints(int N, int[] ringPoints) {
		double sum = 0.0;
		for (int i = 0; i < N + 1; i++) {
			sum += ringPoints[i]
					* (Math.pow((i + 1) / (double) (N + 1), 2.0) - Math.pow(i
							/ (double) (N + 1), 2.0));
		}
		return sum;
	}
}
